﻿using NLog;
using PgsLibApi.Models.Api;
using PgsLibApi.Models.Constants;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace PgsLibApi.Controllers
{

    /// <summary>
    /// End point for file upload See https://github.com/flowjs/ng-flow
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ImagesController : ApiController
    {
        private readonly IFlowService _flowService;
        private readonly ILogger _logger;

        public ImagesController(IFlowService flowService, ILogger logger)
        {
            this._flowService = flowService;
            this._logger = logger;
        }


        /// <summary>
        /// https://github.com/flowjs/ng-flow
        /// </summary>
        /// <param name="flowChunkNumber"></param>
        /// <param name="flowIdentifier"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/image")]
        public IHttpActionResult Upload(int flowChunkNumber,string flowIdentifier)
        {
            if (_flowService.ChunkIsHere(flowChunkNumber, flowIdentifier))
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));  
            }
            else
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
            }
        }


        /// <summary>
        /// https://github.com/flowjs/ng-flow ads
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/image")]
        [ResponseType(typeof(ApiImageResponse))]
        public async Task<IHttpActionResult> Upload()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
                _flowService.CreateFileIfNotExist();
                var provider = new MultipartFormDataStreamProvider(Constants.ImageUrl);
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);
                int chunkNumber = Convert.ToInt32(provider.FormData["flowChunkNumber"]);
                int totalChunks = Convert.ToInt32(provider.FormData["flowTotalChunks"]);
                string identifier = provider.FormData["flowIdentifier"];
                string filename = provider.FormData["flowFilename"];

                // Rename generated file
                MultipartFileData chunk = provider.FileData[0]; 

                _flowService.RenameChunk(chunk, chunkNumber, identifier);

                // Assemble chunks into single file if they're all here
                _flowService.TryAssembleFile(identifier, totalChunks, filename);
                // Success
                _logger.Info(string.Join(" ", "File", Path.Combine(Constants.ImageUrl, filename), "uploaded successful"));
                ApiImageResponse apiImageResponse = new ApiImageResponse() { imageUrl = Path.Combine(Constants.ImageUrl, filename) };
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest)); 
            }
            catch (System.Exception e)
            {
                _logger.Error("", e.Message);
                ApiErrorResponse apiError = new ApiErrorResponse() { error = "Error when proccesing active directory image" };
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
            }
        }

    }
}
