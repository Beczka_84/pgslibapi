﻿using PgsLibApi.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PgsLibApi.Controllers
{

    /// <summary>
    /// Token EndPoint
    /// </summary>
    public class TokenController : ApiController
    {

        /// <summary>
        /// Provides endpoint for token generation
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("token")]
        [ResponseType(typeof(ApiTokenReponse))]
        public IHttpActionResult Token([FromBody]string value)
        {
            //Only to show in documentation
            return null;
        }
    }
}
