﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using NLog;
using PgsLibApi.DAL.Model;
using PgsLibApi.Infrastructure.Linqu;
using PgsLibApi.Models;
using PgsLibApi.Models.Api;
using PgsLibApi.Services;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace PgsLibApi.Controllers
{
    /// <summary>
    /// Controlles for user services
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAdService _adService;
        private readonly IThumbnailService _thumbnailService;
        private readonly ILogger _logger;


        public UsersController(IUserService userService, IAdService adService, IThumbnailService thumbnailService, ILogger logger)
        {
            this._userService = userService;
            this._adService = adService;
            this._thumbnailService = thumbnailService;
            this._logger = logger;
        }


        /// <summary>
        /// Returns user information and user role from active directory
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("users/api/login")]
        [ResponseType(typeof(ApiUserResponse))]
        public IHttpActionResult Login()
        {
            string userLogin = User.Identity.Name;

            if (userLogin == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Invalid token user claim is not presented" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            string userRole = User.Identity.GetUserRole();

            if (userRole == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Invalid token role claim is not presented" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            var result =  _adService.GetUser(userLogin);

            if (result == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Error when searching active directory for a user" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            ApiUserResponse apiUserResponse = new ApiUserResponse();

            User user = new User();

            user.dn = result.Properties["distinguishedname"][0].ToString();
            user.givenName = userLogin;
            user.displayName = result.Properties["displayname"][0].ToString();
            user.sAMAccountName = result.Properties["samaccountname"][0].ToString();
            user.userPrincipalName = result.Properties["userprincipalname"][0].ToString();
            user.role = userRole;

            apiUserResponse.user = user;

            AppUser appUser = _userService.GetUserByEmail(user.userPrincipalName);

            if (appUser == null)
            {
                AppUser appNewUser = Mapper.Map<AppUser>(user);
                ServiceResult addUserResult = _userService.CreateUserAndAddToRole(appNewUser, userRole);

                if (!addUserResult.IsValid)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Error when saving user in db" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, apiUserResponse));
        }

        /// <summary>
        /// Returns photo of a user from active directory
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("users/api/photo")]
        [ResponseType(typeof(ApiImageResponse))]
        public IHttpActionResult Photo()
        {
            string userLogin = User.Identity.Name;

            if (userLogin == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Invalid token user claim is not presented" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            var result = _adService.GetUser(userLogin);

            if (result == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Error when searching active directory for a user" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            ApiImageResponse apiImageResponse = _thumbnailService.saveAndReturnPathToFile(result, x => Url.Content(x) , userLogin);

            if (apiImageResponse == null)
                {
                    ApiErrorResponse apiError = new ApiErrorResponse() { error = "Error when proccesing active directory image" };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
                }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, apiImageResponse));
        }

    }
}
