﻿using PgsLibApi.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;

namespace PgsLibApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public abstract class BaseController : ApiController
    {

        public IHttpActionResult CheckIfObjectIsNull<T>(T value, string errorMsg)
        {
            if(value == null)
            {
                ApiErrorResponse apiError = new ApiErrorResponse() { error = errorMsg };
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, apiError));
            }

            return null;
        }

    }
}
