﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Enums
{
    public enum PropositionsEnum : byte
    {
        Accepted = 1,
        Rejected = 2,
        Reported = 3
    }
}