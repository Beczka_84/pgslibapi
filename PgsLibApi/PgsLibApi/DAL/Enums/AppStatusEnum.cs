﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Enums
{
        public enum AppStatusEnum : byte
        {
            Active = 1,
            Inactive = 2
        }
}