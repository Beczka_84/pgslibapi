﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Model
{
    public class AppCopyFile
    {
        public int ID { get; set; }
        public int CopyID { get; set; }
        public string FilePath { get; set; }

        public AppBookCopy Copy { get; set; }
    }
}