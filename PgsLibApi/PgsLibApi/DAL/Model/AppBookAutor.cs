﻿using PgsLibApi.DAL.Enums;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBookAutor
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AppStatusEnum Status { get; set; }

        public virtual ICollection<AppBook> Books { get; set; }

    }
}