﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Model
{
    public class AppUser
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int AppRoleID { get; set; }

        public AppRole AppRole { get; set; }

        public virtual ICollection<AppBookProposition> BookPropositions { get; set; }
        public virtual ICollection<AppBookQueue> BookQueues { get; set; }
        public virtual ICollection<AppBookCopyLoans> BookCopyLoans { get; set; }

        
    }
}