﻿using PgsLibApi.DAL.Enums;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBookTag
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public AppStatusEnum Status { get; set; }

        public virtual ICollection<AppBook> Book { get; set; }
    }
}