﻿using PgsLibApi.DAL.Enums;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppLocation
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public AppStatusEnum Status { get; set; }

        public virtual ICollection<AppBookCopy> Copys { get; set; }
        public virtual ICollection<AppBookProposition> BookPropositions { get; set; }

    }
}