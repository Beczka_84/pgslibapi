﻿using PgsLibApi.DAL.Enums;
using System;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBookCopyLoans
    {
        public int ID { get; set; }
        public int CopyID { get; set; }
      

        public DateTime DateLoaned { get; set; }
        public int UserID { get; set; }
        public DateTime DateReturned { get; set; }

        public AppStatusEnum Status { get; set; }


        
        public AppUser User { get; set; }

        public AppBookCopy Copy { get; set; }
    }
}