﻿using PgsLibApi.DAL.Enums;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBookCopy
    {
        public int ID { get; set; }
        public int BookID { get; set; }
        public int LocationID { get; set; }

        public AppStatusEnum Status { get; set; }
        public CopyTypeEnum Type { get; set; }
        public int MaxUsers { get; set; }  //Dla papierowej wartość domyślna czyli 1 dla elektronicznej chodzi o licencje

        public virtual ICollection<AppBookCopyLoans> Loans { get; set; }
        public virtual ICollection<AppCopyFile> Files { get; set; }

   
        public AppLocation Location { get; set; }
        public AppBook Book { get; set; }


    }
}