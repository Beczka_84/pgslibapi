﻿using PgsLibApi.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Model
{
    public class AppBookProposition
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int UserID { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }


        public AppStatusEnum Status { get; set; }
        public PropositionsEnum PropositionStatus { get; set; }
        public AppUser User { get; set; }
        public virtual ICollection<AppLocation> Locations { get; set; }
    }
}