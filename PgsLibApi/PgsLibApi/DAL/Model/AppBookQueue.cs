﻿using PgsLibApi.DAL.Enums;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBookQueue
    {
        public int ID { get; set; }
        public int BookID { get; set; }
        public int UserID { get; set; }


        public AppStatusEnum  Status { get; set; }
        public AppUser User { get; set; }
        public AppBook Book { get; set; }
    }
}