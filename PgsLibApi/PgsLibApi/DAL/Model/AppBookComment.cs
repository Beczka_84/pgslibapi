﻿using PgsLibApi.DAL.Enums;

namespace PgsLibApi.DAL.Model
{
    public class AppBookComment
    {
        public int ID { get; set; }
        public decimal Rating { get; set; }
        public string Text { get; set; }
        public AppStatusEnum Status { get; set; }

        public int BookID { get; set; }
        public AppBook Book { get; set; }

    }
}