﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL.Model
{
    public class AppRole
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AppUser> AppUsers { get; set; }

    }
}