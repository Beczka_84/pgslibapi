﻿using PgsLibApi.DAL.Enums;
using System;
using System.Collections.Generic;

namespace PgsLibApi.DAL.Model
{
    public class AppBook
    {
        public int ID { get; set; }
        public string PathToCover { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public int PublishYear { get; set; }
        public AppStatusEnum Status { get; set; }

        public virtual ICollection<AppBookCopy> Copys { get; set; }
        public virtual ICollection<AppBookTag> Tags { get; set; }
        public virtual ICollection<AppBookComment> Comments { get; set; }
        public virtual ICollection<AppBookAutor> Autors { get; set; }



    }
}