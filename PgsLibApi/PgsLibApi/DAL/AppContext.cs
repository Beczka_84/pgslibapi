﻿using Microsoft.AspNet.Identity.EntityFramework;
using PgsLibApi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PgsLibApi.DAL
{
    public class AppContext : DbContext
    {
        public AppContext() : base("AppContext")
        {
            
        }

        public DbSet<AppUser> AppUser { get; set; }
        public DbSet<AppRole> AppRole { get; set; }

        public DbSet<AppBook> Books { get; set; }
        public DbSet<AppBookAutor> BookAutors { get; set; }
        public DbSet<AppBookCopy> BookCopys { get; set; }
        public DbSet<AppBookCopyLoans> CopyLoans { get; set; }
        public DbSet<AppBookQueue> BookQueues { get; set; }
        public DbSet<AppBookProposition> BookPropositions { get; set; }
        public DbSet<AppBookTag> BookTags { get; set; }
        public DbSet<AppBookComment> BookComments { get; set; }
        public DbSet<AppCopyFile> CopyFiles { get; set; }
        public DbSet<AppLocation> Locations { get; set; }
    

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}