﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.Services
{
    public class ServiceResult
    {
        public static ServiceResult Ok()
        {
            return new ServiceResult(true);
        }

        public static ServiceResult Fail(List<KeyValuePair<int, string>> error = null)
        {
            return new ServiceResult(false, error);
        }

        public bool IsValid { get; set; }

        public List<KeyValuePair<int, string>> Errors { get; set; }

        public ServiceResult(bool result, List<KeyValuePair<int, string>> error = null)
        {
            this.Errors = error;
            IsValid = result;
        }
    }
}