﻿using PgsLibApi.DAL.Model;
using System.Collections.Generic;

namespace PgsLibApi.Services.Abstract
{
    public interface IUserService
    {

        bool ValidateUser(string username, string password);

        AppUser GetUserByName(string name);
        AppUser GetUserById(int id);
        AppUser GetUserByEmail(string email);
        ServiceResult CreateUserAndAddToRole(AppUser user, string roleName);


        string GetUserRole(string userName);

        ServiceResult ChangeUserRole(AppUser user, string newRole);
        IList<string> GetAllRoles(string userName);

    }
}
