﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PgsLibApi.Services.Abstract
{
    public interface IFlowService
    {
        string GetChunkFileName(int chunkNumber, string identifier);

        void CreateFileIfNotExist();
        void RenameChunk(MultipartFileData chunk, int chunkNumber, string identifier);
        string GetFileName(string identifier);
        bool ChunkIsHere(int chunkNumber, string identifier);
        bool AllChunksAreHere(string identifier, int totalChunks);
        void TryAssembleFile(string identifier, int totalChunks, string filename);
    }
}
