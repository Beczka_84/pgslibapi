﻿using System.DirectoryServices;

namespace PgsLibApi.Services.Abstract
{
    public interface IAdService
    {
        SearchResult GetUser(string login);
    }
}
