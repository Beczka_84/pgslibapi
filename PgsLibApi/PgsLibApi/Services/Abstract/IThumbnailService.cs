﻿using PgsLibApi.Models.Api;
using System;
using System.DirectoryServices;

namespace PgsLibApi.Services.Abstract
{
    public interface IThumbnailService
    {
        ApiImageResponse saveAndReturnPathToFile(SearchResult result, Func<string,string> predicate, string userLogin);
    }
}
