﻿using PgsLibApi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PgsLibApi.Services.Abstract
{
    public interface IPropositionService
    {
        IQueryable<AppBookProposition> GetAllBooksProposition();
        ServiceResult AddNewBookProposition(AppBookProposition newBookProposition);
        ServiceResult UpdateBookProposition(AppBookProposition newBookProposition);

    }
}
