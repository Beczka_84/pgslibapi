﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using PgsLibApi.DAL;
using PgsLibApi.DAL.Model;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace PgsLibApi.Services.Concrete
{
    public class UserService : IUserService
    {
        private readonly AppContext _db;
        private readonly ILogger _logger;

        [DllImport("ADVAPI32.dll", EntryPoint = "LogonUserW", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        public UserService(AppContext db, ILogger logger)
        {
            this._db = db;
            this._logger = logger;
        }


        public bool ValidateUser(string username, string password)
        {
            IntPtr token = IntPtr.Zero;
            try
            {
                var log = LogonUser(username, "pgs-soft", password, 2, 0, ref token);
                _logger.Info(string.Join(" ",  "User :", username, "Validated :" , log));
                return log;
            }
            catch (Exception e)
            {
                _logger.Error(string.Join(" ", "Error Validating :", username, e.Message));
                return false;
            }
        }

        public AppUser GetUserById(int id)
        {
            return _db.AppUser.FirstOrDefault(x => x.ID == id);
        }

        public AppUser GetUserByName(string name)
        {
            return _db.AppUser.FirstOrDefault(x=>x.Name == name);
        }

        public AppUser GetUserByEmail(string email)
        {
             return _db.AppUser.FirstOrDefault(x=>x.Email == email);
        }

        public ServiceResult CreateUserAndAddToRole(AppUser user, string roleName)
        {
            try
            {
                user.AppRoleID = GetRoleId(roleName);

                _db.AppUser.Add(user);
                _db.SaveChanges();
                _logger.Info(string.Join(" ", "User :", user.Name , "added to db"));

                return ServiceResult.Ok();
            }
            catch (Exception e)
            {
                _logger.Error(string.Join(" ", "Error when adding to db user:", user.Name), e.Message);
                return ServiceResult.Fail();
            }
          
        }

        public int GetRoleId(string name)
        {
            return _db.AppRole.FirstOrDefault(x => x.Name == name).ID ;
        }

        public string GetUserRole(string userName)
        {
            AppUser AppUser = GetUserByName(userName);
            return AppUser.AppRole?.Name ?? string.Empty;
        }

        public ServiceResult ChangeUserRole(AppUser user, string newRole)
        {
            try
            {
                user.AppRoleID = GetRoleId(newRole);
                _db.AppUser.Add(user);
                _db.SaveChanges();
                _logger.Info(string.Join(" ", "Users :", user.Name, "Role was changed successful"));

                return ServiceResult.Ok();
            }
            catch (Exception e)
            {
                _logger.Error(string.Join(" ", "Error when changin role of a user:", user.Name), e.Message);
                return ServiceResult.Fail();
            }
        }

        public IList<string> GetAllRoles(string userName)
        {
            return _db.AppRole.Select(x=>x.Name).ToList();
        }
    }
}