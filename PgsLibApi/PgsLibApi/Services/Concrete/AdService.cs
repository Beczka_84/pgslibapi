﻿using PgsLibApi.Services.Abstract;
using System;
using System.DirectoryServices;
using NLog;

namespace PgsLibApi.Services.Concrete
{
    public class AdService : IAdService
    {
        private readonly ILogger _logger;

        public AdService(ILogger logger)
        {
            this._logger = logger;
        }

        public SearchResult GetUser(string login)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://pgs-soft.com");
                DirectorySearcher search = new DirectorySearcher(entry) { Filter = "(SAMAccountName=" + login + ")" };
                SearchResult resut = search.FindOne();
                _logger.Info(string.Join(" ", "Retrived data from ActiveDirectory for :", login));
                return resut;
            }
            catch (Exception e)
            {
                _logger.Error(string.Join(" ", "Error retriving data from ActiveDirectory for :", login), e.Message);
                return null;
            }
          
        }
    }
}