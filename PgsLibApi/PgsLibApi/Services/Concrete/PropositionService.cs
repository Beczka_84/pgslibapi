﻿using System.Linq;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Web;
using PgsLibApi.DAL.Model;
using NLog;
using PgsLibApi.DAL;
using System.Data.Entity;

namespace PgsLibApi.Services.Concrete
{
    public class PropositionService : IPropositionService
    {

        private readonly AppContext _db;
        private readonly ILogger _logger;
        private readonly IUserService _userService;

        

        public PropositionService(AppContext db, ILogger logger, IUserService userService)
        {
            this._db = db;
            this._logger = logger;
            this._userService = userService;
        }

        public ServiceResult AddNewBookProposition(AppBookProposition newBookProposition)
        {
            try
            {
                _db.BookPropositions.Add(newBookProposition);
                _db.SaveChanges();
                _logger.Info(string.Join(" ", "newBookProposition", _userService.GetUserById(newBookProposition.UserID));
                return ServiceResult.Ok();
            }
            catch (Exception e)
            {
                _logger.Error("Error when adding new book Preposition", e.Message);
                return ServiceResult.Fail();
            }
          
        }

        public IQueryable<AppBookProposition> GetAllBooksProposition()
        {
            return _db.BookPropositions;
        }

        public ServiceResult UpdateBookProposition(AppBookProposition newBookProposition)
        {
            try
            {
                _db.BookPropositions.Attach(newBookProposition);
                _db.Entry(newBookProposition).State = EntityState.Modified;
                _db.SaveChanges();

                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}