﻿using NLog;
using PgsLibApi.Models.Api;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace PgsLibApi.Services.Concrete
{
    public class ThumbnailService : BaseService, IThumbnailService
    {

        private readonly ILogger _logger;

        public ThumbnailService(ILogger logger)
        {
            this._logger = logger;
        }

        public ApiImageResponse saveAndReturnPathToFile(SearchResult result, Func<string, string> predicate, string userLogin)
        {

            string pathToFile = HttpContext.Current.Server.MapPath("~/Thumbnails/" + userLogin + ".png");


            bool exists = Directory.Exists(HttpContext.Current.Server.MapPath("~/Thumbnails/"));

            if (!exists)
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Thumbnails/"));


            if (File.Exists(pathToFile))
            {
                _logger.Info(string.Join(" ", "Retrived data user thumbnail for :", userLogin));
                return new ApiImageResponse() { imageUrl = predicate("~/Thumbnails/" + userLogin + ".png") };
            }
            try
            {
                byte[] data = result.Properties["thumbnailPhoto"][0] as byte[];

                if (data != null)
                {
                    using (MemoryStream s = new MemoryStream(data))
                    {
                        var img = Bitmap.FromStream(s);
                        img.Save(pathToFile, ImageFormat.Png);
                    }
                }
                _logger.Info(string.Join(" ", "Retrived data user thumbnail for :", userLogin));
                return new ApiImageResponse() { imageUrl = predicate("~/Thumbnails/" + userLogin + ".png") };
            }
            catch (Exception e)
            {
                _logger.Error(string.Join(" ", "Error retriving user thumbnail image  for :", userLogin), e.Message);
                return null;
            }
        }
    }
}