namespace PgsLibApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class db : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        AppRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppRoles", t => t.AppRoleID, cascadeDelete: false)
                .Index(t => t.AppRoleID);
            
            CreateTable(
                "dbo.AppBookCopyLoans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CopyID = c.Int(nullable: false),
                        DateLoaned = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                        DateReturned = c.DateTime(nullable: false),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppBookCopies", t => t.CopyID, cascadeDelete: false)
                .ForeignKey("dbo.AppUsers", t => t.UserID, cascadeDelete: false)
                .Index(t => t.CopyID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AppBookCopies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        LocationID = c.Int(nullable: false),
                        Status = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false),
                        MaxUsers = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppBooks", t => t.BookID, cascadeDelete: false)
                .ForeignKey("dbo.AppLocations", t => t.LocationID, cascadeDelete: false)
                .Index(t => t.BookID)
                .Index(t => t.LocationID);
            
            CreateTable(
                "dbo.AppBooks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PathToCover = c.String(),
                        Title = c.String(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        PublishYear = c.Int(nullable: false),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppBookAutors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppBookComments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Rating = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Text = c.String(),
                        Status = c.Byte(nullable: false),
                        BookID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppBooks", t => t.BookID, cascadeDelete: false)
                .Index(t => t.BookID);
            
            CreateTable(
                "dbo.AppBookTags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppCopyFiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CopyID = c.Int(nullable: false),
                        FilePath = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppBookCopies", t => t.CopyID, cascadeDelete: false)
                .Index(t => t.CopyID);
            
            CreateTable(
                "dbo.AppLocations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppBookPropositions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        UserID = c.Int(nullable: false),
                        Link = c.String(),
                        Description = c.String(),
                        Status = c.Byte(nullable: false),
                        PropositionStatus = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppUsers", t => t.UserID, cascadeDelete: false)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AppBookQueues",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AppBooks", t => t.BookID, cascadeDelete: false)
                .ForeignKey("dbo.AppUsers", t => t.UserID, cascadeDelete: false)
                .Index(t => t.BookID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AppBookAutorAppBooks",
                c => new
                    {
                        AppBookAutor_ID = c.Int(nullable: false),
                        AppBook_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AppBookAutor_ID, t.AppBook_ID })
                .ForeignKey("dbo.AppBookAutors", t => t.AppBookAutor_ID, cascadeDelete: false)
                .ForeignKey("dbo.AppBooks", t => t.AppBook_ID, cascadeDelete: false)
                .Index(t => t.AppBookAutor_ID)
                .Index(t => t.AppBook_ID);
            
            CreateTable(
                "dbo.AppBookTagAppBooks",
                c => new
                    {
                        AppBookTag_ID = c.Int(nullable: false),
                        AppBook_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AppBookTag_ID, t.AppBook_ID })
                .ForeignKey("dbo.AppBookTags", t => t.AppBookTag_ID, cascadeDelete: false)
                .ForeignKey("dbo.AppBooks", t => t.AppBook_ID, cascadeDelete: false)
                .Index(t => t.AppBookTag_ID)
                .Index(t => t.AppBook_ID);
            
            CreateTable(
                "dbo.AppBookPropositionAppLocations",
                c => new
                    {
                        AppBookProposition_ID = c.Int(nullable: false),
                        AppLocation_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AppBookProposition_ID, t.AppLocation_ID })
                .ForeignKey("dbo.AppBookPropositions", t => t.AppBookProposition_ID, cascadeDelete: false)
                .ForeignKey("dbo.AppLocations", t => t.AppLocation_ID, cascadeDelete: false)
                .Index(t => t.AppBookProposition_ID)
                .Index(t => t.AppLocation_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AppBookQueues", "UserID", "dbo.AppUsers");
            DropForeignKey("dbo.AppBookQueues", "BookID", "dbo.AppBooks");
            DropForeignKey("dbo.AppBookCopyLoans", "UserID", "dbo.AppUsers");
            DropForeignKey("dbo.AppBookCopies", "LocationID", "dbo.AppLocations");
            DropForeignKey("dbo.AppBookPropositions", "UserID", "dbo.AppUsers");
            DropForeignKey("dbo.AppBookPropositionAppLocations", "AppLocation_ID", "dbo.AppLocations");
            DropForeignKey("dbo.AppBookPropositionAppLocations", "AppBookProposition_ID", "dbo.AppBookPropositions");
            DropForeignKey("dbo.AppBookCopyLoans", "CopyID", "dbo.AppBookCopies");
            DropForeignKey("dbo.AppCopyFiles", "CopyID", "dbo.AppBookCopies");
            DropForeignKey("dbo.AppBookTagAppBooks", "AppBook_ID", "dbo.AppBooks");
            DropForeignKey("dbo.AppBookTagAppBooks", "AppBookTag_ID", "dbo.AppBookTags");
            DropForeignKey("dbo.AppBookCopies", "BookID", "dbo.AppBooks");
            DropForeignKey("dbo.AppBookComments", "BookID", "dbo.AppBooks");
            DropForeignKey("dbo.AppBookAutorAppBooks", "AppBook_ID", "dbo.AppBooks");
            DropForeignKey("dbo.AppBookAutorAppBooks", "AppBookAutor_ID", "dbo.AppBookAutors");
            DropForeignKey("dbo.AppUsers", "AppRoleID", "dbo.AppRoles");
            DropIndex("dbo.AppBookPropositionAppLocations", new[] { "AppLocation_ID" });
            DropIndex("dbo.AppBookPropositionAppLocations", new[] { "AppBookProposition_ID" });
            DropIndex("dbo.AppBookTagAppBooks", new[] { "AppBook_ID" });
            DropIndex("dbo.AppBookTagAppBooks", new[] { "AppBookTag_ID" });
            DropIndex("dbo.AppBookAutorAppBooks", new[] { "AppBook_ID" });
            DropIndex("dbo.AppBookAutorAppBooks", new[] { "AppBookAutor_ID" });
            DropIndex("dbo.AppBookQueues", new[] { "UserID" });
            DropIndex("dbo.AppBookQueues", new[] { "BookID" });
            DropIndex("dbo.AppBookPropositions", new[] { "UserID" });
            DropIndex("dbo.AppCopyFiles", new[] { "CopyID" });
            DropIndex("dbo.AppBookComments", new[] { "BookID" });
            DropIndex("dbo.AppBookCopies", new[] { "LocationID" });
            DropIndex("dbo.AppBookCopies", new[] { "BookID" });
            DropIndex("dbo.AppBookCopyLoans", new[] { "UserID" });
            DropIndex("dbo.AppBookCopyLoans", new[] { "CopyID" });
            DropIndex("dbo.AppUsers", new[] { "AppRoleID" });
            DropTable("dbo.AppBookPropositionAppLocations");
            DropTable("dbo.AppBookTagAppBooks");
            DropTable("dbo.AppBookAutorAppBooks");
            DropTable("dbo.AppBookQueues");
            DropTable("dbo.AppBookPropositions");
            DropTable("dbo.AppLocations");
            DropTable("dbo.AppCopyFiles");
            DropTable("dbo.AppBookTags");
            DropTable("dbo.AppBookComments");
            DropTable("dbo.AppBookAutors");
            DropTable("dbo.AppBooks");
            DropTable("dbo.AppBookCopies");
            DropTable("dbo.AppBookCopyLoans");
            DropTable("dbo.AppUsers");
            DropTable("dbo.AppRoles");
        }
    }
}
