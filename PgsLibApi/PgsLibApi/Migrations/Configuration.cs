namespace PgsLibApi.Migrations
{
    using Models;
    using DAL.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<PgsLibApi.DAL.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PgsLibApi.DAL.AppContext context)
        {

            var role1 = new AppRole { ID = (int)AppRolesEnum.Admin,  Name = AppRolesEnum.Admin.ToString() };
            var role2 = new AppRole { ID = (int)AppRolesEnum.User, Name = AppRolesEnum.User.ToString() };

            context.AppRole.AddOrUpdate(role1);
            context.AppRole.AddOrUpdate(role2);
        }
    }
}
