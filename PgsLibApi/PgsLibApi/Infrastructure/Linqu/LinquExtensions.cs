﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace PgsLibApi.Infrastructure.Linqu
{
    public static class LinquExtensions
    {
            public static string GetUserRole(this IIdentity identity)
            {
                return ((ClaimsIdentity)identity).FindFirst(ClaimTypes.Role)?.Value ?? string.Empty;
            }
    }
}