﻿using AutoMapper;
using PgsLibApi.Infrastructure.AutoMapper.Users;

namespace PgsLibApi.Infrastructure.AutoMapper
{
    /// <summary>
    /// Provides Automapper configuration
    /// </summary>
    public static class AutoMapperConfiguration
    {

        /// <summary>
        /// Configure AutoMapper Profiles
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ApiUsersProfile>();
            });
        }
    }
}