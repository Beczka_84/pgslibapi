﻿using AutoMapper;
using PgsLibApi.DAL.Model;
using PgsLibApi.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.Infrastructure.AutoMapper.Users
{

    /// <summary>
    /// Auto mapper profilee for mappings in api/users
    /// </summary>
    public class ApiUsersProfile : Profile
    {
        /// <summary>
        /// Mappings
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<User, AppUser>()
               .ForMember(x => x.Email, opt => opt.MapFrom(y => y.userPrincipalName))
               .ForMember(x => x.Name, opt => opt.MapFrom(y => y.sAMAccountName));
        }

    }
}