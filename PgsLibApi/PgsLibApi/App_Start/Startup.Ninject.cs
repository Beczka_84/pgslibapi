﻿using Microsoft.Owin.Security;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using Ninject.Extensions.Conventions;
using System.Web;
using System.Web.Http;
using Ninject.Modules;
using System.Reflection;
using Owin;
using PgsLibApi.DAL;
using Microsoft.AspNet.Identity;
using PgsLibApi.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Microsoft.Owin.Security.OAuth;
using PgsLibApi.Providers;
using Ninject.Web.Common;
using PgsLibApi.Services.Concrete;
using PgsLibApi.Infrastructure;
using PgsLibApi.Providers.Abstract;
using PgsLibApi.Providers.Concrete;
using NLog;

namespace PgsLibApi.App_Start
{
    public partial class Startup
    {
        public IKernel ConfigureNinject(IAppBuilder app)
        {
            var kernel = CreateKernel();
           
            
            return kernel;
        }

        public IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<AppContext>().ToSelf().InRequestScope();

            kernel.Bind(x => { x.From(typeof(UserService).Assembly).SelectAllClasses().EndingWith("Service").BindDefaultInterface(); });

            kernel.Bind<IOAuthAuthorizationServerProvider>().To<AuthorizationServerProvider>();
            kernel.Bind<IOAuthAuthorizationServerOptions>().To<MyOAuthAuthorizationServerOptions>();

            kernel.Bind<ILogger>().ToMethod(x=> LogManager.GetCurrentClassLogger());


            return kernel;
        }
    }

    public class NinjectConfig : NinjectModule
    {
        public override void Load()
        {
            RegisterServices();
        }

        private void RegisterServices()
        {
            //kernel.Load(Assembly.GetExecutingAssembly());
        }
    }
}