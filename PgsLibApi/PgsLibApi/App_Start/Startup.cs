﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using PgsLibApi.DAL;
using PgsLibApi.Infrastructure;
using PgsLibApi.Infrastructure.AutoMapper;
using PgsLibApi.Providers;
using PgsLibApi.Providers.Abstract;
using PgsLibApi.Providers.Concrete;
using PgsLibApi.Services.Abstract;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

[assembly: OwinStartup(typeof(PgsLibApi.App_Start.Startup))]

namespace PgsLibApi.App_Start
{
    public partial class Startup
    {
        public static HttpConfiguration config { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();
            config.EnableCors();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
       
            var kernel = ConfigureNinject(app);

            ConfigureOAuth(app, kernel);
            config.DependencyResolver = new NinjectResolver(kernel);

            //app.UseWebApi(config);
            app.UseNinjectMiddleware(() => kernel);
            app.UseNinjectWebApi(config);


            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfiguration.Configure();
        }

        public void ConfigureOAuth(IAppBuilder app, IKernel kernel)
        {
            // Token Generation
            //app.UseOAuthAuthorizationServer(kernel.Get<MyOAuthAuthorizationServerOptions>().GetOptions());
            //app.UseOAuthBearerAuthentication();

            app.UseOAuthBearerTokens(kernel.Get<IOAuthAuthorizationServerOptions>().GetOptions());
        }
    }
}
