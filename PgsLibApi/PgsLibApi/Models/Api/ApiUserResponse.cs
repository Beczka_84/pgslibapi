﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.Models.Api
{
    public class ApiUserResponse
    {
       public User user { get; set; }
    }

    public class User
    {
        public string dn { get; set; }
        public string givenName { get; set; }
        public string displayName { get; set; }
        public string sAMAccountName { get; set; }
        public string userPrincipalName { get; set; }

        public string role { get; set; }
    }
}