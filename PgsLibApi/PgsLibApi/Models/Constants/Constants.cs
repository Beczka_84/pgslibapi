﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PgsLibApi.Models.Constants
{
    public class Constants
    {
        public static string ImageUrl = Path.Combine(HttpContext.Current.Server.MapPath("~"), "FileUpload");
    }
}