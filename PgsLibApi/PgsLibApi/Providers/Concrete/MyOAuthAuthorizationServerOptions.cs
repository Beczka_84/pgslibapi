﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using PgsLibApi.Providers.Abstract;
using PgsLibApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PgsLibApi.Providers.Concrete
{
    public class MyOAuthAuthorizationServerOptions : IOAuthAuthorizationServerOptions
    {
        private IOAuthAuthorizationServerProvider _provider;

        public MyOAuthAuthorizationServerOptions(IOAuthAuthorizationServerProvider provider)
        {
            this._provider = provider;
        }

        public OAuthAuthorizationServerOptions GetOptions()
        {
            return new OAuthAuthorizationServerOptions ()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = _provider
            };
        }
    }
}