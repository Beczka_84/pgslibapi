﻿using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Web.Common;
using PgsLibApi.DAL.Model;
using PgsLibApi.Models;
using PgsLibApi.Services.Abstract;
using PgsLibApi.Services.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace PgsLibApi.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {

        private readonly IUserService _userService;

        public AuthorizationServerProvider(IUserService userService)
        {
            this._userService = userService;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

         
                bool result = _userService.ValidateUser((string)context.UserName, (string)context.Password);

                if (!result)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    context.Response.StatusCode = 401;
                    return;
                }

            string appUserRole = _userService.GetUserRole(context.UserName);

            if (appUserRole == null) appUserRole = AppRolesEnum.User.ToString();

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, appUserRole));
            context.Validated(identity);

        }
    }
}